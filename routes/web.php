<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/siswa','SiswaController@index')->name('siswa');
Route::get('/create-siswa','SiswaController@create')->name('create-siswa');
Route::get('/simpan-siswa','SiswaController@store')->name('simpan-siswa');
